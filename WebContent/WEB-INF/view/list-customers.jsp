<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>

<html>
<head>
<title>List Customers</title>
<!-- Reference to css -->
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/style.css">
</head>
<body>
<div id="wrapper">
	<div id="header">
		<h2>CRM - Customer Relationship Manager</h2>
	</div>
</div>

<div id="container">
	<div id="content">
	
		<!-- buttons -->
		<input type="button" value="Add customer" class="add-button"  
			onclick="window.location.href='showFormForAdd'; return false;"/>
		<form:form action="search" method="post">
			Search customer(s):
			<input type="text" name="searchString"/>
			<input type="submit" value="Search" class="add-button"/>
			<a href="${pageContext.request.contextPath }/customer/list">Clear filter</a>
		</form:form>
		<table> 	
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Action</th>
			</tr>
		<c:forEach var="var" items="${customers }" varStatus="iter">
			<c:url var="updateLink" value="/customer/showFormForUpdate">
				<c:param name="customerId" value="${var.id }"/>
			</c:url>
			<c:url var="deleteLink" value="/customer/delete">
				<c:param name="customerId" value="${var.id }"/>
			</c:url>
			<tr>
				<td>${var.firstName }</td>
				<td>${var.lastName }</td>
				<td>${var.email }</td>
				<td><a href="${updateLink }">Update</a> | 
					<a href="${deleteLink }" onclick="if (!(confirm('Are you sure you wanna delete this customer'))) return false;">Delete</a>
				</td>
			</tr>
		</c:forEach>
		</table>
	</div>
</div>
</body>
</html>