package com.luv2code.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.luv2code.springdemo.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	//injecting session factory
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Customer> getCustomers() {
		//get current hibernate session
		Session session = sessionFactory.getCurrentSession();
		
		Query<Customer> query = session.createQuery("from Customer order by lastName", Customer.class);
		
		List<Customer> customers = query.getResultList();
		
		return customers;
	}

	@Override
	public void saveCustomer(Customer customer) {
		
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(customer);
	}

	@Override
	public Customer getCustomer(int id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Customer.class, id);
	}

	@Override
	public void deleteCustomer(int id) {
		//get current session
		Session session = sessionFactory.getCurrentSession();
		
		//create delete query
		Query query = session.createQuery("delete from Customer where id=:idCustomer");
		//set parameter customerid in query
		query.setParameter("idCustomer", id);
		
		//execute query
		query.executeUpdate();
	}

	@Override
	public List<Customer> getCustomers(String searchString) {
		Session session = sessionFactory.getCurrentSession();
		Query<Customer> query = session.createQuery("from Customer where firstName=:searchString "
				+ "or lastName=:searchString", Customer.class);
		query.setParameter("searchString", searchString);
		List<Customer> customers = query.list();
		return customers;
	}

}
