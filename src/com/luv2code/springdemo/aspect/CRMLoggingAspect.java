package com.luv2code.springdemo.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CRMLoggingAspect {

	private static Logger logger = Logger.getLogger(CRMLoggingAspect.class.getClass().getName());
	
	@Pointcut("execution(* com.luv2code.springdemo.controller.*.*(..))")
	private void forControllerPackage(){}
	
	@Pointcut("execution(* com.luv2code.springdemo.service.*.*(..))")
	private void forServicePackage(){}
	
	@Pointcut("execution(* com.luv2code.springdemo.dao.*.*(..))")
	private void forDaoPackage(){}
	
	@Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
	private void forNotEntityPackage(){}
	
	@Before("forNotEntityPackage()")
	public void beforeAspect(JoinPoint joinPoint){
		
		String method = joinPoint.getSignature().toShortString();
		logger.info("====> @Before calling method: " + method);
		
		Object [] args = joinPoint.getArgs();
		for (Object o : args){
			logger.info("====> Argument: " + o);
		}
	}
	
	@AfterReturning(pointcut="forNotEntityPackage()", returning="result")
	public void afterReturningAspect(JoinPoint joinPoint, Object result){
		String method = joinPoint.getSignature().toShortString();
		logger.info("====> @AfterReturning calling method: " + method);
		
		System.out.println("====> Returned value" + result);
	}
}
